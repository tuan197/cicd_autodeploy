FROM node:18-alpine

WORKDIR /app

RUN npm install -g pm2

# RUN addgroup --system --gid 1001 mygroup
# RUN adduser --system --uid 1001 myuser

# RUN chown -R myuser:mygroup /app

# USER myuser

# COPY --chown=myuser:mygroup ["package.json", "package-lock.json*", "./"]
COPY ["package.json", "package-lock.json*", "./"]

RUN npm install --silent

# COPY --chown=myuser:mygroup . .
COPY . .

RUN chown -R node:node /app

USER node

RUN mkdir -p .docker/data/db
RUN chown -R node:node .docker/data/db

RUN export NODE_OPTIONS=--no-experimental-fetch
RUN pm2 install pm2-logrotate 

CMD ["pm2-runtime", "ecosystem.config.js"]
